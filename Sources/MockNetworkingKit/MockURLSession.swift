import Foundation

///
///
///
public class MockURLSession: URLSession {

	// ===================================
	// MARK: Public API
	// ===================================

	///
	///
	///
	public init(data: Data, response: URLResponse? = nil) {
		MockURLProtocol.mockData = data
		MockURLProtocol.mockResponse = response
		MockURLProtocol.mockError = nil
		super.init(configuration: mockConfiguration)
	}

	///
	///
	///
	public init(response: URLResponse) {
		MockURLProtocol.mockData = nil
		MockURLProtocol.mockResponse = response
		MockURLProtocol.mockError = nil
		super.init(configuration: mockConfiguration)
	}

	///
	///
	///
	public init(error: Error) {
		MockURLProtocol.mockData = nil
		MockURLProtocol.mockResponse = nil
		MockURLProtocol.mockError = error
		super.init(configuration: mockConfiguration)
	}

	// ===================================
	// MARK: Private implementation
	// ===================================

	// The mock configuartion used
	private var mockConfiguration: URLSessionConfiguration = {
		let config = URLSessionConfiguration()
		config.protocolClasses = [MockURLProtocol.self]
		return config
	}()
}