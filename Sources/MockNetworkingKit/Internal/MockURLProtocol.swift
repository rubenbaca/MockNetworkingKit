import Foundation

///
/// Mock/dummy `URLProtocol` class that returns (loads) whatever you tell it to load.
///
/// Set the type properties `mockResponse` and `mockData` to whatever you want to have loaded.
/// To have the loading fail, set the type property `mockError` to the error of your choice
///
internal class MockURLProtocol: URLProtocol {

	// ====================
	// MARK: Public API
	// ====================

	///
	/// `startLoading()` will call `urlProtocol(_, didReceive: URLResponse)` with the value you 
	/// provide here.
	///
	static var mockResponse: URLResponse?

	///
	/// `startLoading()` will call `urlProtocol(_, didLoad: Data)` with the value you 
	/// provide here.
	/// 
	static var mockData: Data?

	///
	/// `startLoading()` will call `urlProtocol(_, didFailWithError: Error)` with the value you 
	/// provide here.
	///
	static var mockError: Error?

	// ================================
	// MARK: Adopting URLProtocol
	// ================================

	//
	// Can init with request
	//
	override class func canInit(with request: URLRequest) -> Bool {
		return true
	}
	
	//
	// Start loading
	//
	override func startLoading() {

		if let error = MockURLProtocol.mockError { 
			client?.urlProtocol(self, didFailWithError: error)
			return
		}

		if let data = MockURLProtocol.mockData {
			client?.urlProtocol(self, didLoad: data)
		}

		if let response = MockURLProtocol.mockResponse {
			client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
		}

		client?.urlProtocolDidFinishLoading(self)
	}
}