import XCTest

@testable import MockNetworkingKit

//
//
//
final class MockURLSessionTests: TestCase {
	
	// ==========================
	// Private mock/dummy data
	// ==========================
	private struct MockError: Error, LocalizedError { }
	private let mockURL: URL = URL(string: "https://example.com")!
	private let mockError: Error = MockError()
	private let mockData: Data = "Mock data...".data(using: .utf8)!
	private lazy var mockResponse: URLResponse = HTTPURLResponse(url: mockURL, statusCode: 200, httpVersion: nil, headerFields: nil)!

	// MARK: Tests

	//
	// Tests a `dataTask(with:)` that returns an error
	//
	func test_dataTask_init_error() {

		let session = MockURLSession(error: mockError)

		session.dataTask(with: mockURL) { data, response, error in			
			XCTAssertNil(data)
			XCTAssertNil(response)
			XCTAssertNotNil(error)
			XCTAssertEqual(error?.localizedDescription, self.mockError.localizedDescription)

			self.signal()
		}.resume()

		wait()
	}

	//
	// Tests a `dataTask(with:)` that returns data only
	//
	func test_dataTask_init_data() {

		let session = MockURLSession(data: mockData)

		session.dataTask(with: mockURL) { data, response, error in			
			XCTAssertNil(response)
			XCTAssertNil(error)
			XCTAssertNotNil(data)
			XCTAssertEqual(data, self.mockData)

			self.signal()
		}.resume()

		wait()
	}

	//
	// Tests a `dataTask(with:)` that returns a url-response
	//
	func test_dataTask_init_response() {

		let session = MockURLSession(response: mockResponse)

		session.dataTask(with: mockURL) { data, response, error in			

			guard let data = data, data.isEmpty else { return XCTFail("Expected data to be not nil and empty") }
			
			XCTAssertNil(error)
			XCTAssertNotNil(response)
			XCTAssertEqual(response, self.mockResponse)
			XCTAssertEqual(response?.url, self.mockURL)

			self.signal()
		}.resume()

		wait()
	}

	//
	// Tests a `dataTask(with:)` that returns both: data and url-reponse
	//
	func test_dataTask_init_data_response() {

		let session = MockURLSession(data: mockData, response: mockResponse)

		session.dataTask(with: mockURL) { data, response, error in			
			
			XCTAssertNil(error)
			XCTAssertNotNil(data)
			XCTAssertNotNil(response)
			XCTAssertEqual(data, self.mockData)
			XCTAssertEqual(response, self.mockResponse)
			XCTAssertEqual(response?.url, self.mockURL)

			self.signal()
		}.resume()

		wait()
	}

	// MARK: Other

    static var allTests = [
        ("test_dataTask_init_error", test_dataTask_init_error),
        ("test_dataTask_init_data", test_dataTask_init_data),
        ("test_dataTask_init_response", test_dataTask_init_response),
        ("test_dataTask_init_data_response", test_dataTask_init_data_response),
    ]
}



















//
// Doing this since I can't use test-expectations in linux (swift 4.1), like this:
// 
// https://developer.apple.com/documentation/xctest/asynchronous_tests_and_expectations/testing_asynchronous_operations_with_expectations 
//
// I'll just use signal() and wait() for testing async. tasks, using a `DispatchSemaphore`
//
class TestCase: XCTestCase {
	private var semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
	func signal() { semaphore.signal() }
	func wait() { semaphore.wait() }
}