import XCTest

import MockNetworkingKitTests

var tests = [XCTestCaseEntry]()
tests += MockNetworkingKitTests.allTests()
XCTMain(tests)